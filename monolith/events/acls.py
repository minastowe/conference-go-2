from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    # Do the request
    url = 'https://api.pexels.com/v1/search'
    payload = {
        'query': f'{city} {state}',
        'per_page': 1,
    }
    response = requests.get(url, params=payload, headers=headers)
    # Pull out the data from the request
    photo_dict = json.loads(response.content)
    try:
        return photo_dict['photos'][0]['url']
    except (KeyError, IndexError):
        return None


def get_lat_lon(location):
    params = {
        "q": f'{location.city}, {location.state.abbreviation}, USA',
        "appid": OPEN_WEATHER_API_KEY
    }

    # Get the latitude and longitude from the response
    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response

def get_weather_data(location):
    lat_lon = get_lat_lon(location)
    if lat_lon is None:
        return None
    
    # Create the URL for the geocoding API with the city and state
    url = 'https://api.openweathermap.org/data/2.5/weather'
    # Make the request
    params = {
        "lat": lat_lon["lat"],
        "lon": lat_lon["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }

    response = requests.get(url, params)
    # Parse the JSON response
    weather_description = response.json()["weather"][0]["description"]
    temp = response.json()["main"]["temp"]

    return {
        "description": weather_description,
        "temp": temp
    }

    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary