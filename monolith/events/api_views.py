from django.http import JsonResponse
from encoders.json import ModelEncoder, LocationListEncoder, LocationDetailEncoder, ConferenceListEncoder, ConferenceDetailEncoder, ConferenceListEncoder
from events.models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET": # GET ALL CONFERENCES
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            safe=False
        )
    else: # POST
        content = json.loads(request.body)
        try:
            # Grabs the location for Conference from Model location before creating new conference
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET": # LIST SPECIFIC CONFERENCE
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE": # DELETE
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: # PUT
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        # Updates conference
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET": # GET ALL LOCATIONS
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False
        )
    else:  # POST
        content = json.loads(request.body)
        try:
            # Grabs the abbreviation for state from Model State before creating location
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        photo_url = get_photo(content['city'], state.abbreviation)
        content['picture_url'] = photo_url
        # Creates new location
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET": # GET SPECIFIC LOCATION
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE": # DELETE
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: # PUT
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        
        #Need to create a error handling here for location details id not existing
        # Updates location
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )